<?php ob_start(); //啟動系統緩重區?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>金幣探測器</title>
        <style>
            @import url(css/page3_table.css);
            
            body {
                padding: 20px 0;
                background-image: url('media/錢幣.jpg');
                background-size: 90%, 120%;
            }
            h3{color: black;}
        </style>
        <script src="https://code.jquery.com/jquery-2.1.4.js"></script>

    </head>
    <body>
        <div align="left">
        <?php 
            $account = $_COOKIE["Account"];
            echo "<h3>帳號:".$account."<br></h3>";
        ?>
        </div>

        <div align="center">
        <?php
            $user = $_POST['account'];

            $link = @mysqli_Connect('localhost','id19773421_root','S95362wl1234567_','id19773421_money');
            mysqli_set_charset($link,"uft8");

            $sql_cost = "select Date,Product,Price,Unit,Count,Sort from cost_record where Account=".'"'.$user.'"'." ORDER BY Date;";
            $sql_sales = "select Date,Product,Price,Unit,Count from sales_record where Account=".'"'.$user.'"'." ORDER BY Date;";

            $result_cost = mysqli_query($link,$sql_cost) or die(mysqli_error($link));
            $result_sales = mysqli_query($link,$sql_sales) or die(mysqli_error($link));

            cost_table_creat($result_cost,$user);
            sales_table_creat($result_sales,$user);

            function cost_table_creat($result_cost,$user){
                echo("<table id='tbList'>");
                echo("<caption>".$user."的成本紀錄</caption>");
                echo('<thead><tr><th scope="col">日期</th><th scope="col">產品名稱</th><th scope="col">每單位價錢</th><th scope="col">單位</th><th scope="col">數量</th><th scope="col">分類</th></tr></thead>');
                echo('<tbody>');
                
                while($list=mysqli_fetch_array($result_cost)){
                    echo('<tr>');
                    echo('<td>'.$list['Date'].'</td>');
                    echo('<td>'.$list['Product'].'</td>');
                    echo('<td>'.$list['Price'].'</td>');
                    echo('<td>'.$list['Unit'].'</td>');
                    echo('<td>'.$list['Count'].'</td>');
                    echo('<td>'.$list['Sort'].'</td>');
                    echo('</tr>');
                }
                echo('</tbody>');
                echo("<tr><td colspan='6'><a href = 'Ma_page1.php'>返回管理者畫面</a></td></tr>");
                echo("</table>");
            }

            function sales_table_creat($result_sales,$user){
                echo("<table id='tbList'>");
                echo("<caption>".$user."的銷售紀錄</caption>");
                echo('<thead><tr><th scope="col">日期</th><th scope="col">產品名稱</th><th scope="col">每單位價錢</th><th scope="col">單位</th><th scope="col">銷售數量</th></tr></thead>');
                echo('<tbody>');
                
                while($list=mysqli_fetch_array($result_sales)){
                    echo('<tr>');
                    echo('<td>'.$list['Date'].'</td>');
                    echo('<td>'.$list['Product'].'</td>');
                    echo('<td>'.$list['Price'].'</td>');
                    echo('<td>'.$list['Unit'].'</td>');
                    echo('<td>'.$list['Count'].'</td>');
                    echo('</tr>');
                }
                echo('</tbody>');
                echo("<tr><td colspan='5'><a href = 'Ma_page1.php'>返回管理者畫面</a></td></tr>");
                echo("</table>");
            }
            
         
            //echo "日期"."\t/\t"."產品名稱"."\t/\t"."每單位價錢"."\t/\t"."單位"."\t/\t"."分類".'<br>';

            /*$json = array();

            while($list=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                echo $list['Date']."<br>";
                $json += array(
                    "Date" => $list['Date'],
                    "Product" => $list['Product'],
                    "Price" => $list['Price'],
                    "Unit" => $list['Unit'],
                    "Sort" => $list['Sort']
                );
            }
            $response = json_encode($json);
            echo $response."<br>";
            */
        ?>
        </div>
    </body>
</html>