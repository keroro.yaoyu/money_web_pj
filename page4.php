<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>金幣探測器</title>
    <style>
        @import url(css/nav_style.css);

        html {
            box-sizing: border-box;
        }

        *,
        *::before,
        *::after {
            box-sizing: inherit;
        }

        body {
            padding: 20px 0;
            background-image: url('media/公園.jpg');
            /*background-image: url('media/山2.jpg');*/
            background-size: 100%, 200%;
        }

        h1,
        h2 {
            margin: 0;
            padding: 34px 0 14px 40px;
        }

        #connect {
            margin: 0px auto;
            background-color: rgba(255, 255, 255, 0.9);
        }
    </style>
 
    <script src="Chartjs/Chart.bundle.js"></script>
    <script src="Chartjs/utils.js"></script>
</head>

<body bgcolor="#d4cbba">
    <div align="left" id='account_show_area'>
        <?php 
            $account = $_COOKIE["Account"];
            echo "<h3>帳號:".$account."<br></h3>";
        ?>
    </div>
    <nav>
        <ul class="flex-nav">
            <li><a href="page1.html">
                    成本紀錄
                </a></li>
            <li><a href="page2.html">
                    銷售紀錄
                </a></li>
            <li><a href="page3.html">
                    查詢資料
                </a></li>
            <li><a href="page4.php">
                    統計圖
                </a></li>
        </ul>
    </nav>
    <br>
    <br>

    <?php
        $account = $_COOKIE["Account"];
        $link = @mysqli_Connect('localhost','id19773421_root','S95362wl1234567_','id19773421_money');
        mysqli_set_charset($link,"uft8");
        
        $sql_cost = 'select Date,Product,Price,Unit,Count,Sort from cost_record where Account="'.$account.'"'." ORDER BY Date".";";
        $sql_sales = 'select Date,Product,Price,Unit,Count from sales_record where Account="'.$account.'"'." ORDER BY Date".";";

        $result_cost = mysqli_query($link,$sql_cost) or die(mysqli_error($link));
        $result_sales = mysqli_query($link,$sql_sales) or die(mysqli_error($link));

        $cost_arr=array();
        $sales_arr=array();

        while($k=mysqli_fetch_assoc($result_cost)){
            array_push($cost_arr,$k);
        }
        while($k=mysqli_fetch_assoc($result_sales)){
            array_push($sales_arr,$k);
        }
        
        $cost_arr_js= json_encode($cost_arr);
        $sales_arr_js= json_encode($sales_arr);

        echo "<div align='center'><input type='button' name='insert' id='insert' value='獲取資料' onClick='add_Data($cost_arr_js,$sales_arr_js)'></div><br>";
        


    ?>
    <div align='center'>
    <select id="seletmonth">
        <option value="" disabled>請選擇月份</option>
        <option value="一月">一月</option>
        <option value="二月">二月</option>
        <option value="三月">三月</option>
        <option value="四月">四月</option>
        <option value="五月" >五月</option>
        <option value="六月">六月</option>
        <option value="七月">七月</option>
        <option value="八月">八月</option>
        <option value="九月">九月</option>
        <option value="十月">十月</option>
        <option value="十一月">十一月</option>
        <option value="十二月">十二月</option>
    </select>
    </div>
    <br>
    <br>
    <div align='center'>
        <a href="page4.php">獲利圖</a> / <a href="page4_2.php">損益表</a>
    </div>
    <br>
    <br>
    <div style="width:75%;" id="connect">
        <canvas id="canvas"></canvas>
    </div>
    

    <script>
        var fix_cost_t=[0,0,0,0,0,0,0];//固定成本累進總額(藍)
        var vari_cost_t=[0,0,0,0,0,0,0];//變動成本累進總額(綠)
        var vari_cost=[0,0,0,0,0,0,0];//各時段的變動成本
        var sales_t=[0,0,0,0,0,0,0];//銷售額累進總額(黃)
        var profit=[0,0,0,0,0,0,0];//各時段的銷售額
        var small_profit=[0,0,0,0,0,0,0];//微利額累進總額(紅)
            

        function add_Data(cost_arr_js,sales_arr_js){

            update();
            var seletmonth = document.getElementById('seletmonth');
            console.log(cost_arr_js);
            console.log(sales_arr_js);
            console.log(seletmonth.value);
             Analysis_Data(cost_arr_js,sales_arr_js,seletmonth.value);
            

             console.log("fix_cost_t="+fix_cost_t);
             console.log("vari_cost_t="+vari_cost_t);
             console.log("vari_cost="+vari_cost);
             console.log("sales_t="+sales_t);
             console.log("profit="+profit);
             console.log("small_profit="+small_profit);

            show_chart(seletmonth.value);

            seletmonth.hidden = true;


        };
        
        function Analysis_Data(cost,sales,seletmonth){
            var is_data = false;

            for(var i=0;i<cost.length;i++){//成本紀錄

                var month = cost[i]['Date'].substr(5,2);
                console.log("month="+month);

                if(check_month(month)==seletmonth){

                    is_data = true;
                    var day = cost[i]['Date'].substr(8,2);
                    
                    var k =check_day(day);

                    if(cost[i]['Sort']=='固定成本'){
                        if(k!=0){
                            for(var j=0;j<=k;j++){
                                if(fix_cost_t[j]==0){
                                    fix_cost_t[j] += fix_cost_t[j-1];
                                }
                            }
                        }
                        fix_cost_t[k]+=cost[i]['Price']*cost[i]['Count'];

                    }else if(cost[i]['Sort']=='變動成本'){
                        vari_cost[k]+=(cost[i]['Price']*cost[i]['Count']);
                        console.log("k="+k);
                           
                        if(k!=0){
                            for(var j=0;j<=k;j++){
                                if(vari_cost_t[j]==0){
                                    vari_cost_t[j] += vari_cost_t[j-1];
                                }
                            }
  
                        }

                        vari_cost_t[k]+=cost[i]['Price']*cost[i]['Count'];
                        
                    }
                }
            }
            if(is_data===false){
                for(var i=0;i<7;i++){
                    
                    fix_cost_t[i]=0;
                    vari_cost[i]=0;
                    vari_cost_t[i]=0;
                }
            }else{
                is_data = false;
            }

            
            for(var i=0;i<sales.length;i++){//銷售紀錄

                var month = sales[i]['Date'].substr(5,2);

                if(check_month(month)==seletmonth){
                    is_data = true;
                    var day = sales[i]['Date'].substr(8,2);
                    var k =check_day(day);

                    if(k!=0 && sales_t[k]==0){
                        sales_t[k] += sales_t[k-1];
                    }
                    sales_t[k]+=sales[i]['Price']*sales[i]['Count'];
                    profit[k]+=sales[i]['Price']*sales[i]['Count']; 
                }
            }
            if(is_data===false){
                for(var i=0;i<7;i++){
                    
                    sales_t[i]=0;
                    profit[i]=0;
                }
            }

            for(var i=0;i<7;i++){//計算微利額
                if(i!=0 && small_profit[i]==0){
                    small_profit[i] += small_profit[i-1];
                }
                small_profit[i] += profit[i]-vari_cost[i];
            }

            for(var i=0;i<7;i++){
                if(is_data === true && fix_cost_t[i]==0){
                    fix_cost_t[i] = fix_cost_t[i-1];
                }
                if(is_data === true && vari_cost_t[i]==0){
                    vari_cost_t[i] = vari_cost_t[i-1];
                }
                if(is_data === true && sales_t[i]==0){
                    sales_t[i] = sales_t[i-1];
                }
                if(is_data === true && small_profit[i]==0){
                    small_profit[i] = small_profit[i-1];
                }
            }
            is_data = false;


        }

        function check_month(month){
            if(month=='01'){
                return '一月';
            }else if(month=='02'){
                return '二月';
            }else if(month=='03'){
                return '三月';
            }else if(month=='04'){
                return'四月';
            }else if(month=='05'){
                return'五月';
            }else if(month=='06'){
                return'六月';
            }else if(month=='07'){
                return'七月';
            }else if(month=='08'){
                return'八月';
            }else if(month=='09'){
                return'九月';
            }else if(month=='10'){
                return'十月';
            }else if(month=='11'){
                return'十一月';
            }else if(month=='12'){
                return'十二月';
            }
        }

        function check_day(day){
            var day = parseInt(day);

            if(1==day){
                return 0;
            }else if(day<=5){
                return 1;
            }else if(day<=10){
                return 2;
            }else if(day<=15){
                return 3;
            }else if(day<=20){
                return 4;
            }else if(day<=25){
                return 5;
            }else if(day<=31){
                return 6;
            }
        }



        
        
        
        function show_chart(seletmonth){
            
            var config = {
            
            type: 'line',
            data: {
                
                labels: ['1號', '5號', '10號', '15號', '20號', '25號', check_dayOfMonth(seletmonth)],
                datasets: [{
                    label: '微利額',
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    
                    data: [
                        small_profit[0],
                        small_profit[1],
                        small_profit[2],
                        small_profit[3],
                        small_profit[4],
                        small_profit[5],
                        small_profit[6]
                        
                    ],
                    fill: false,
                }, {
                    label: '固定成本',
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: [
                        fix_cost_t[0],
                        fix_cost_t[1],
                        fix_cost_t[2],
                        fix_cost_t[3],
                        fix_cost_t[4],
                        fix_cost_t[5],
                        fix_cost_t[6]
                    ],
                }, {
                    label: '銷售項目成本',
                    fill: false,
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.green,
                    data: [
                        vari_cost_t[0],
                        vari_cost_t[1],
                        vari_cost_t[2],
                        vari_cost_t[3],
                        vari_cost_t[4],
                        vari_cost_t[5],
                        vari_cost_t[6]
                    ],
                }, {
                    label: '銷售額',
                    fill: false,
                    backgroundColor: window.chartColors.yellow,
                    borderColor: window.chartColors.yellow,
                    data: [
                        sales_t[0],
                        sales_t[1],
                        sales_t[2],
                        sales_t[3],
                        sales_t[4],
                        sales_t[5],
                        sales_t[6]
                    ],
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: '獲利圖'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: '日期'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: '金流的金額'
                        }
                    }]
                }
            }
            };

            var ctx = document.getElementById('canvas').getContext('2d');
            var chart = new Chart(ctx, config);


            function check_dayOfMonth(seletmonth){
                if(seletmonth=='一月' || seletmonth=='三月' || seletmonth=='五月' || seletmonth=='七月' || seletmonth=='八月' || seletmonth=='十月' || seletmonth=='十二月'){
                    return '31號';
                }else if(seletmonth=='四月' || seletmonth=='六月' || seletmonth=='九月' || seletmonth=='十一月'){
                    return '30號';
                }else{
                    return '28號';
                }
            }
        }

        function update(){
            for(var i=0;i<7;i++){
                fix_cost_t[i]=0;
                vari_cost_t[i]=0;
                vari_cost[i]=0;
                sales_t[i]=0;
                profit[i]=0;
                small_profit[i]=0;
            }
        }

        
        
    </script>

    
</body>

</html>