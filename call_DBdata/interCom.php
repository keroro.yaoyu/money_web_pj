<?php

header('Content-Type: application/json; charset=utf8'); 
class Response{
/* interger $Status 狀態碼 200/400
* string $Msg 提示資訊
* array $Data 資料
* return string 返回值  json返回的資料
* */
public static function json($Status,$Msg,$Data=array()){
if(!is_numeric($Status)){ //是否為數字
return "";
}
//組裝好新的資料
$result=array(
'Status'=>$Status,
'Msg'=>$Msg,
'Data'=>$Data
);
//變成json格式的
echo json_encode($result,JSON_UNESCAPED_UNICODE);//JSON_UNESCAPED_UNICODE讓中文不編碼
exit;
}
}
?>