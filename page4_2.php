<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <title>金幣探測器</title>
    <style>
        @import url(css/nav_style.css);
        @import url(css/page3_table.css);

        html {
            box-sizing: border-box;
        }

        *,
        *::before,
        *::after {
            box-sizing: inherit;
        }

        body {
            padding: 20px 0;
            background-image: url('media/公園.jpg');
            /*background-image: url('media/山2.jpg');*/
            background-size: 100%, 200%;
        }

        h1,
        h2 {
            margin: 0;
            padding: 34px 0 14px 40px;
        }

        #connect {
            margin: 0px auto;
            background-color: rgba(255, 255, 255, 0.9);
        }
    </style>

    <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
 
</head>

<body bgcolor="#d4cbba">
    <div align="left" id='account_show_area'>
        <?php 
            $account = $_COOKIE["Account"];
            echo "<h3>帳號:".$account."<br></h3>";
        ?>
    </div>
    <nav>
        <ul class="flex-nav">
            <li><a href="page1.html">
                    成本紀錄
                </a></li>
            <li><a href="page2.html">
                    銷售紀錄
                </a></li>
            <li><a href="page3.html">
                    查詢資料
                </a></li>
            <li><a href="page4.php">
                    統計圖
                </a></li>
        </ul>
    </nav>
    <br>
    <br>

    <?php
        $account = $_COOKIE["Account"];
        $link = @mysqli_Connect('localhost','id19773421_root','S95362wl1234567_','id19773421_money');
        mysqli_set_charset($link,"uft8");
        
        $sql_cost = 'select Date,Product,Price,Unit,Count,Sort from cost_record where Account="'.$account.'"'." ORDER BY Date".";";
        $sql_sales = 'select Date,Product,Price,Unit,Count from sales_record where Account="'.$account.'"'." ORDER BY Date".";";

        $result_cost = mysqli_query($link,$sql_cost) or die(mysqli_error($link));
        $result_sales = mysqli_query($link,$sql_sales) or die(mysqli_error($link));

        $cost_arr=array();
        $sales_arr=array();

        while($k=mysqli_fetch_assoc($result_cost)){
            array_push($cost_arr,$k);
        }
        while($k=mysqli_fetch_assoc($result_sales)){
            array_push($sales_arr,$k);
        }
        
        $cost_arr_js= json_encode($cost_arr);
        $sales_arr_js= json_encode($sales_arr);

        echo "<div align='center'><input type='button' name='insert' id='insert' value='獲取資料' onClick='add_Data($cost_arr_js,$sales_arr_js)'></div><br>";
        


    ?>
    <div align='center'>
    <select id="seletmonth">
        <option value="" disabled>請選擇月份</option>
        <option value="一月">一月</option>
        <option value="二月">二月</option>
        <option value="三月">三月</option>
        <option value="四月">四月</option>
        <option value="五月" >五月</option>
        <option value="六月">六月</option>
        <option value="七月">七月</option>
        <option value="八月">八月</option>
        <option value="九月">九月</option>
        <option value="十月">十月</option>
        <option value="十一月">十一月</option>
        <option value="十二月">十二月</option>
    </select>
    </div>
    <br>
    <br>
    <div align='center'>
        <a href="page4.php">獲利圖</a> / <a href="page4_2.php">損益表</a>
    </div>
    <br>
    <br>

    <div align='center'>
        <table id="cost_flow">
            <tr style="background-color:rgb(255, 233, 204);">
                <th colspan='3'>固定成本</th>
            </tr>

            <tbody id="fix_cost">
            <tr>
                <th>日期</th>
                <th>成本項目</th>
                <th>價錢</th>
            </tr>
            </tbody>

            <tr>
                <th colspan='3' id="fix_cost_t">小計:</th>
            </tr>

            <tr style="background-color:rgb(255, 233, 204);">
                <th colspan='3'>變動成本</th>
            </tr>

            <tbody id="vir_cost">
            <tr>
                <th>日期</th>
                <th>成本項目</th>
                <th>價錢</th>
            </tr>
            </tbody>

            <tr>
                <th colspan='3' id="vir_cost_t">小計:</th>
            </tr>

            <tr style="background-color:#fcc;">
                <th colspan='3' id="cost_t">成本總計:</th>
            </tr>

            
        </table>
    </div>

    <br>
    <br>
    
    <div align='center'>
        <table id="sale_flow">
            <tr style="background-color:rgb(255, 233, 204);">
                <th colspan='2'>銷售額</th>
            </tr>

            <tbody id="sale">
            <tr>
                <th>銷售項目</th>
                <th>項目總銷售額</th>
            </tr>
            </tbody>

            <tr style="background-color:#fcc;">
                <th colspan='2' id="sales_t">銷售額總計:</th>
            </tr>    
        </table>
    </div>

    <br>
    <br>

    <div align='center'>
        <table id="sale_flow">
            <tr style="background-color:rgb(255, 233, 204);">
                <th colspan='2'>淨利</th>
            </tr>

            <tbody id="profit">
                <th colspan='2' id="profit_t">淨利總計:</th>
            </tbody>
   
        </table>
    </div>

    <script>
        var sales=[0,0,0,0,0,0,0];

            

        function add_Data(cost_arr_js,sales_arr_js){
            $("#seletmonth").hide();
            
            var seletmonth = $("#seletmonth").val();

            table_creat(cost_arr_js,sales_arr_js,seletmonth);

        };

        function table_creat(cost,sale,seletmonth){
            var cost_t = 0;
            var sales_t = 0;
            var profit = 0;

            cost_t=fixcost_creat(cost,seletmonth)+vircost_creat(cost,seletmonth);
            
            $("#cost_t").text("成本總計:"+cost_t);

            sales_t=sale_creat(sale,seletmonth);

            $("#sales_t").text("銷售額總計:"+sales_t);

            profit = sales_t-cost_t;
            $("#profit_t").text("淨利總計:"+profit);

        }

        function fixcost_creat(cost,seletmonth){
            var total = 0;
            for(var i=0;i<cost.length;i++){//成本紀錄

                var month = cost[i]['Date'].substr(5,2);  
                
                if(check_month(month)==seletmonth){
                    
                    if(cost[i]['Sort']=='固定成本'){
                        var $tr = $(document.createElement('tr'));
                        $tr.appendTo("#fix_cost");

                        var $td = $(document.createElement('td'));
                        $td.text(cost[i]['Date']);
                        $td.appendTo($tr);

                        var $td = $(document.createElement('td'));
                        $td.text(cost[i]['Product']);
                        $td.appendTo($tr);

                        var price = cost[i]['Price']*cost[i]['Count'];
                        total+=price;
                        var $td = $(document.createElement('td'));
                        $td.text(price);
                        $td.appendTo($tr);
                    }
                }
            }
            $("#fix_cost_t").text("小計:"+total);
            return total;
        }

        function vircost_creat(cost,seletmonth){
            var total = 0;
            for(var i=0;i<cost.length;i++){//成本紀錄

                var month = cost[i]['Date'].substr(5,2);  
                
                if(check_month(month)==seletmonth){
                    
                    if(cost[i]['Sort']=='變動成本'){
                        var $tr = $(document.createElement('tr'));
                        $tr.appendTo("#vir_cost");

                        var $td = $(document.createElement('td'));
                        $td.text(cost[i]['Date']);
                        $td.appendTo($tr);

                        var $td = $(document.createElement('td'));
                        $td.text(cost[i]['Product']);
                        $td.appendTo($tr);

                        var price = cost[i]['Price']*cost[i]['Count'];
                        total+=price;
                        var $td = $(document.createElement('td'));
                        $td.text(price);
                        $td.appendTo($tr);
                    }
                }
            }
            $("#vir_cost_t").text("小計:"+total);
            return total;
        }

        function sale_creat(sale,seletmonth){
            var total = 0;
            var sale_arr=[];
            var sale_m = [];
            for(var i=0;i<sale.length;i++){//銷售紀錄
                var month = sale[i]['Date'].substr(5,2); 
                
                if(check_month(month)==seletmonth){

                    console.log("qq"+sale_arr.includes(sale[i]['Product']));
                    
                    if(!sale_arr.includes(sale[i]['Product'])){
                        sale_arr.push(sale[i]['Product']);
                        sale_m.push(0);
                    }
                    
                    for(var j=0;j<sale_arr.length;j++){
                        if(sale_arr[j]==sale[i]['Product']){
                            sale_m[j]+=sale[i]['Price']*sale[i]['Count'];
                            console.log(sale_m[j]);
                        }
                    }
                    
                }
                
            }

            for(var i=0;i<sale_arr.length;i++){
                var $tr = $(document.createElement('tr'));
                $tr.appendTo("#sale");

                var $td = $(document.createElement('td'));
                $td.text(sale_arr[i]);
                $td.appendTo($tr);

                var $td = $(document.createElement('td'));
                $td.text(sale_m[i]);
                $td.appendTo($tr);

                total+=sale_m[i];
            }
            
            return total;
        }

        
           
        function check_month(month){
            if(month=='01'){
                return '一月';
            }else if(month=='02'){
                return '二月';
            }else if(month=='03'){
                return '三月';
            }else if(month=='04'){
                return'四月';
            }else if(month=='05'){
                return'五月';
            }else if(month=='06'){
                return'六月';
            }else if(month=='07'){
                return'七月';
            }else if(month=='08'){
                return'八月';
            }else if(month=='09'){
                return'九月';
            }else if(month=='10'){
                return'十月';
            }else if(month=='11'){
                return'十一月';
            }else if(month=='12'){
                return'十二月';
            }
        }

        
        
    </script>

    
</body>

</html>