<?php ob_start(); //啟動系統緩重區?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>金幣探測器</title>
        <style>
            @import url(css/page3_table.css);
            
            body {
                padding: 20px 0;
                background-image: url('media/商店街.jpg');
                background-size: 100%, 200%;
            }
            h3{color: white;}

            #title{
                font-weight:bold;
                font-size:20px;
            }

            #dele_O_area{width:300px;margin:0 auto; background: rgb(255, 233, 204);}
        </style>
        <script src="https://code.jquery.com/jquery-2.1.4.js"></script>

    </head>
    <body>
        <div align="left" id='account_show_area'>
        <?php 
            $account = $_COOKIE["Account"];
            echo "<h3>帳號:".$account."<br></h3>";
        ?>
        </div>

        <div align="center" id="dele_O_area">
            <fieldset >
                <p id="title">資料刪除</p>
                <form id="search_list" method="post" action="delete_connect.php" onSubmit="return check()">
                    <p>日期：<input name="date" id="date" type="text" size="8"></p>
                    <p>銷售項目：<input name="product" id="product" type="text" size="8"></p>
                    <p><input  type="hidden" name="type" id="sales_record" value="sales_record"><p>
                    <input type="submit" name="del" id='del' value="刪除">
                </form>
            </fieldset>
        </div>

        <div align="center">
        <?php
            $year = date("Y");
            $month = $_POST['selectmonth_content'];
            $date = $_POST['date'];
            $date_dat = explode('/',$date);
            $product = $_POST['product'];
            $price = $_POST['price'];
            $unit = $_POST['unit'];
            $count = $_POST["count"];

            $select_nonull = array();
            foreach($_POST as $key => $value){
                
                if($value != null && $key!="search"){
                    $select_nonull += array($key=>$value);
                }
            }
            //echo "<br>";
            //print_r($select_nonull);
            //table_creat(count($select_nonull),$select_nonull);
            echo "<br>";

            $link = @mysqli_Connect('localhost','id19773421_root','S95362wl1234567_','id19773421_money');
            mysqli_set_charset($link,"uft8");
            $sql = "select Date,Product,Price,Unit,Count from sales_record where ";
            $con = count($select_nonull);

            foreach($select_nonull as $key => $value){
                $con-=1;
                if($key=='selectmonth_content'){
                    if($value=='12'){
                        $sql=$sql."Date".">=".'"'.$year."-".$value.'"'." and ";
                    }else{
                        $value2 = $value+1;
                        $sql=$sql."Date".">=".'"'.$year."-".$value.'-01"'." and "."Date"."<".'"'.$year."-".$value2.'-01"'." and ";
                    }
                    
                }else if($key=='date'){
                    $sql=$sql.$key."=".'"'.$year."-".$date_dat[0]."-".$date_dat[1].'"'." and ";
                }else{
                    $sql=$sql.$key."=".'"'.$value.'"'." and ";
                }
            }
            $sql=$sql."Account=".'"'.$account.'"'." ORDER BY Date".";";
            //echo $sql;
            $result = mysqli_query($link,$sql) or die(mysqli_error($link));

            table_creat($result);

            function table_creat($result){
                echo("<table id='tbList'>");
                echo('<thead><tr><th scope="col">日期</th><th scope="col">銷售項目</th><th scope="col">每單位價錢</th><th scope="col">單位</th><th scope="col">銷售數量</th></tr></thead>');
                echo('<tbody>');
                
                while($list=mysqli_fetch_array($result)){
                    echo('<tr>');
                    echo('<td>'.$list['Date'].'</td>');
                    echo('<td>'.$list['Product'].'</td>');
                    echo('<td>'.$list['Price'].'</td>');
                    echo('<td>'.$list['Unit'].'</td>');
                    echo('<td>'.$list['Count'].'</td>');
                    echo('</tr>');
                }
                echo('</tbody>');
                echo("<tr><td colspan='5'><a href = 'page3_2.html'>返回銷售紀錄畫面</a></td></tr>");
                echo("</table>");
            }
            
         
            //echo "日期"."\t/\t"."產品名稱"."\t/\t"."每單位價錢"."\t/\t"."單位"."\t/\t"."分類".'<br>';

            /*$json = array();

            while($list=mysqli_fetch_array($result,MYSQLI_ASSOC)){
                echo $list['Date']."<br>";
                $json += array(
                    "Date" => $list['Date'],
                    "Product" => $list['Product'],
                    "Price" => $list['Price'],
                    "Unit" => $list['Unit'],
                    "Sort" => $list['Sort']
                );
            }
            $response = json_encode($json);
            echo $response."<br>";
            */
        ?>
        </div>

        <script>
                function check() {
                    var date = $("#date").val();
                    var product = $("#product").val();
                    if(date=="" && product==""){
                        alert("日期和產品名稱不可皆空白");
                        return false;
                    }else{
                        return true;
                    }
                }
        </script>
    </body>
</html>